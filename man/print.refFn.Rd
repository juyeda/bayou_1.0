\name{print.refFn}
\alias{print.refFn}
\title{S3 method for printing refFn objects}
\usage{
  \method{print}{refFn} (x, ...)
}
\arguments{
  \item{x}{A function of class 'refFn' produced by
  make.refFn}

  \item{...}{Additional arguments passed to \code{print}}
}
\description{
  S3 method for printing refFn objects
}

