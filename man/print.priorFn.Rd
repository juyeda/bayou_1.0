\name{print.priorFn}
\alias{print.priorFn}
\title{S3 method for printing priorFn objects}
\usage{
  \method{print}{priorFn} (x, ...)
}
\arguments{
  \item{x}{A function of class 'priorFn' produced by
  \code{make.prior}}

  \item{...}{Additional arguments passed to \code{print}}
}
\description{
  S3 method for printing priorFn objects
}

